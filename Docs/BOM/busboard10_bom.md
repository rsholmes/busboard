# busboard10.kicad_sch BOM

2024-08-23T21:54:26-0400

Generated from schematic by Eeschema 8.0.4-8.0.4-0~ubuntu22.04.1

**Component Count:** 17


## 
| Refs | Qty | Component | Description | Manufacturer | Part | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- | ---- | ---- |
| +12v1, -12v1 | 2 | LED | Light emitting diode |  |  | Tayda | A-1553 |
| J1–12 | 12 | Synth_power_2x5_narrow | 2x5 IDC connector (Eurorack style power header) |  |  | Tayda | A-2939 |
| J13 | 1 | Conn_01x03 | Screw terminal | Degson | DG301 | Tayda | A-669 |
| R1, R2 | 2 | 2K | Resistor |  |  | Tayda |  |

### Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|2K|2|R1, R2|

