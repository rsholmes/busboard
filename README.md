# Eurorack/Kosmo ±12 V Compact Busboard

Power distribution busboard with twelve 2x5 pin connectors, supplying ±12V only (no +5 V).

Size 25 x 200 mm.

**NOTE:** The August 2024 revision *has not been tested*. I don't expect any problems but *absolutely do check for shorts, incorrect connections, and so on before connecting any power supply or module!*

![picture](Images/busboard10.png)

[Image of earlier version]
